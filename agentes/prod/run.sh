#!/bin/bash
set -o nounset

export JENKINS_CONTAINER_NAME=prod
export JENKINS_HOME=/home/jenkins
export JENKINS_AGENT_SSH_PUBKEY="$(cat ~/.ssh/id_rsa.pub)"

JENKINS_CONTAINER_NAME=$JENKINS_CONTAINER_NAME JENKINS_HOME=$JENKINS_HOME JENKINS_AGENT_SSH_PUBKEY="$JENKINS_AGENT_SSH_PUBKEY" docker-compose -f docker-compose.yaml up --build -d

sleep 10

# Here we have just installed tools that help us create a python virtual environment for our agent node, install as many tools as you require
docker exec $JENKINS_CONTAINER_NAME bash -c "apt-get update -y -q && apt-get upgrade -y -q && apt-get install -y -q git python3 python3-venv maven"
