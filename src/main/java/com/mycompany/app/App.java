package com.mycompany.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Hello world!
 */
public class App {

    private static final String MESSAGE = "Hello World!";

    public App() {
    }

    public static void main(String[] args) {
        System.out.println("Message: " + getMessage());
        System.out.println("Time: " + getDate());
	System.out.println("Java Runtime: " + System.getProperty("java.version"));
    }

    public static final String getMessage() {
        return MESSAGE;
    }

    public static final String getDate() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return format1.format(cal.getTime());

    }
}
