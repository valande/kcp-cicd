# KeepCoding DevOps 7 CI/CD Práctica Final

En esta práctica pondremos a prueba los conocimientos adquiridos en el módulo de CI/CD del Bootcamp de DevOps de KeepCoding.

**NOTA**: En esta práctica se da cierta flexibilidad al estudiante para realizar ciertas tareas. Es decir, hay una parte de la práctica que no tiene una única solución y que depende del criterio del estudiante elegir una u otra opción.

El objetivo de esta práctica es montar un proyecto similar a lo que se piden en una empresa a los profesionales de DevOps.

# Requisitos
1. Se desea definir un flujo de CI/CD en Jenkins que posea los requisitos de calidad exigidos. Para ello, se debe desarrollar y probar una aplicación que siga los estándares de calidad para poder ser deplegado en producción. Dicha aplicación (y todo el contenido) se guardará en un repositorio de GitHub. Cualquier cambio en dicho repositorio disparará el pipeline de Jenkins.
2. La aplicación, cuyos artefactos genera el pipeline, será definida por el usuario. Por ejemplo, puede ser una página web, una aplicación Java, C++, etc.
3. Se deberá realizar una compilación, ejecución y pruebas en local de la aplicación, para probar que realmente funciona como se espera (lo cual se realizará con un *Makefile*).
4. Las credenciales utilizadas en el pipeline nunca estarán guardadas en el código.
5. Se deberán utilizar dos nodos externos (en Jenkins), cada uno de los cuales utiliza un agente Jenkins distinto. Estos nodos se podrán utilizar para la ejecución de las tareas de Jenkins. Dichos nodos pueden ser o bien máquinas virtuales o contenedores Docker ejecutando un agente de Jenkins (mejor opción).
6. El flujo de despliegue será "Continuous Delivery" en la rama `main`, es decir, dejaremos los artefactos correspondientes listos para desplegar en producción.
7. [OPCIONAL] En otras ramas distintas de `main`, el despliegue será "Continuous Deployment" y no habrá aprobación manual, será totalmente automático.
8. Finalmente, se quiere que cada 30 minutos se ejecuten los tests de la rama `main`, para poder tener un seguimiento del estado de los mismos.


# Entregables
> A continuación se describen los entregables que se tendrán en cuenta para evaluar la práctica. Lo más típico es que se entreguen todos juntos en un mismo proyecto de GitHub.

1. Código fuente del producto que se despliega, incluyendo tests de prueba.
2. *Makefile* para despliegue local con *README.md* con instrucciones para los desarrolladores.
3. Un fichero de texto *nodo_jenkins.txt* o *nodo_jenkins.doc* donde se describa qué tipo de nodo/agente y la configuración que se ha utilizado para ejecutar los jobs de Jenkins. En caso de utilizar Docker (opción preferible), incluir los ficheros de Docker (o docker-compose) necesarios para ejecutar el contenedor (con las instrucciones para ejecutarlo). En caso de utilizar una VM, indicar los pasos que se han seguido para ejecutar el agente en dicha máquina y la configuración de la máquina.
4. Incluir en el fichero *nodo_jenkins.txt* o *nodo_jenkins.doc* anterior la configuración utilizada en Jenkins para el agente (se pueden adjuntar imágenes).
5. *Jenkinsfile* del job de despliegue.
6. Describir cómo se ha definido la tarea periódica donde se especifica la acción recurrente de ejecución de tests cada 30 minutos.
6. *README.md* donde se especificará que versión de instancia de Jenkins se ha utilizado así como los plugins, secretos y configuraciones necesarias.
7. *README.md* (el mismo que el anterior) explicando el proyecto y las decisiones de diseño tomadas. Explicar el porqué de las mismas, sus beneficios y sus inconvenientes.


# Solución

En este proyecto se ha desarrollado una aplicación muy sencilla, similar a la desarrollada en clase. Se trata de un proyecto en _Java_ que imprime un mensaje de saludo, la fecha y hora cuando se ejecuta la aplicación, y la versión del Runtime de _Java_. Además, se han desarrollado unos tests unitarios que prueban los métodos (funciones en _Java_) que se definen para la aplicación. Esta aplicación, debido a su sencillez, no tiene dependencias de librerías complejas, simplemente utiliza las estándar de _Java_. Por eso, es compatible con cualquier versión del Runtime de _Java_. De hecho, se va a reflejar una discrepancia entre entornos de desarrollo y test/prod, donde el Runtime de _Java_ es distinto. Esto se ha hecho así porque a veces los entornos de desarrollo difieren de los productivos. Por otro lado, y como veremos, el entorno de test es similar al de producción (por la sencillez del proyecto, no hemos definido un entorno de pre-producción).

Un despliegue de la aplicación simplemente consiste en copiar el paquete de _Java_ (un fichero _.jar_ y ejecutarlo). En la fase de pruebas, se lanzan tests los tests unitarios definidos en la fase de desarrollo. Esto se ha hecho así porque el proyecto no interactúa con otros componentes. Si fuese así, deberíamos definir tests funcionales, de integración, etc. y ejecutarlos en la fase y entorno de pruebas. En este caso, se simula este caso ejecutando los mismos tests pero en un entorno de pruebas que difiere de desarrollo.

A continuación, mostramos las características técnicas necesarias para probar el proyecto en completitud, incluyendo:

* características de la máquina donde se ha desarrollado el proyecto
* versión de Jenkins, librerías y plugins
* pasos para la configuración del proyecto (asumiendo que tenemos una máquina con Jenkins instalado)
* descripción del pipeline definido y de la salida del mismo

## Ficheros/directorios incluidos

* **src**: Código fuente del proyecto. Se trata de un proyecto bastante sencillo escrito en _Java_ que muestra un mensaje, la hora y la versión del runtime de _Java_.
* **Makefile**: Fichero para simplificar la compilación, ejecución y generación de artefactos de modo local.
* **pom.xml**: Fichero de configuración para poder ejecutar _Maven_ con el proyecto.
* **Jenkinsfile**: Fichero que define el pipeline de CI/CD de Jenkins.
* **agentes**: Scripts para poder ejecutar los agentes de Jenkins en forma de contenedores.
* **nodo_jenkins.doc**: Descripción de los agentes de Jenkins y configuraciones.
* **README_solucion.md**: Reemplaza al _README.md_ que se pide en la práctica, porque el _README.md_ corresponde al enunciado de la práctica.

## Detalles de la máquina
* Máquina Ubuntu Linux 22.04, con 2 CPUs, 8 GB de RAM y 12GB de disco, y acceso a internet
* Jenkins 2.387.2 (y un usuario jenkins con directorio raíz en /home/jenkins, perteneciente al grupo de docker y con permisos de administrador). **Nota**: si no se especifica lo contrario, se usa este usario para ejecutar los comandos desde el terminal
* OpenJDK 17.0.3
* Docker y docker-compose
* GNU make 4.3
* Maven 3.8.6

## Plugins de Jenkins

De la lista de plugins a continuación, mencionar que no todos son necesarios. De esta lista cabe mencionar los plugins de BlueOcean, desde donde se ha definido el Pipeline de Jenkins (aunque se han añadido las acciones _post_ del pipeline a mano), los plugins de Github (para poder conectarnos a Github), los plugins de Java, Maven y Junit (necesarios para realizar las acciones del Pipeline), los plugins de SCM para trabajar con repositorios de código fuente, SSH Agent plugin, y Job DSL (para definir la tarea periódica). El resto de plugins los he utilizado en alguna ocasión anterior, se pueden utilizar para definir pipelines fuera de BlueOcean, son "helpers" para determinadas tareas, o mejoran la experiencia para la gestión, monitorización y/o visualización de trabajos y tareas de Jenkins.

* Authentication Tokens API Plugin: Version 1.53.v1c90fd9191a_b_
* Blue Ocean: Version 1.27.3
* Blue Ocean Pipeline Editor: Version 1.27.3
* Branch API Plugin: Version 2.1071.v1a_188a_562481
* Build History Manager: Version 1.7.0
* Build Timeout: Version 1.30
* Checks API plugin: Version 2.0.0
* Command Agent Launcher Plugin: Version 90.v669d7ccb_7c31
* Common API for Blue Ocean: Version 1.27.3
* Config API for Blue Ocean: Version 1.27.3
* Credentials Binding Plugin: Version 604.vb_64480b_c56ca_
* Credentials Plugin: Version 1224.vc23ca_a_9a_2cb_0
* Dashboard for Blue Ocean: Version 1.27.3
* Dashboard View: Version 2.472.v9ff2a_e6a_c529
* Design Language: Version 1.27.3
* Disable GitHub Multibranch Status Plugin: Version 1.2
* Display URL API: Version 2.3.7
* Display URL for Blue Ocean: Version 2.4.1
* Docker API Plugin: Version 3.2.13-68.va_875df25a_b_45
* Docker Commons Plugin: Version 419.v8e3cd84ef49c
* Docker Pipeline: Version 563.vd5d2e5c4007f
* Docker plugin: Version 1.3.0
* Durable Task Plugin: Version 504.vb10d1ae5ba2f
* Events API for Blue Ocean: Version 1.27.3
* Favorite: Version 2.4.1
* Git Changelog: Version 3.29
* Git client plugin: Version 4.2.0
* Git Parameter Plug-In: Version 0.9.18
* Git Pipeline for Blue Ocean: Version 1.27.3
* Git plugin: Version 5.0.0
* Git Push Plugin: Version 34.vd474e0fe7b_ec
* Git server Plugin: Version 99.va_0826a_b_cdfa_d
* Git Tag Message Plugin: Version 1.7.1
* GitHub API Plugin: Version 1.303-417.ve35d9dd78549
* GitHub Authentication plugin: Version 0.39
* GitHub Branch Source Plugin: Version 1703.vd5a_2b_29c6cdc
* GitHub Checks plugin: Version 1.0.19
* GitHub Commit Skip SCM Behaviour: Version 0.4.0
* Github Custom Notification Context SCM Behaviour: Version 1.1
* GitHub Integration Plugin: Version 0.5.0
* GitHub Pipeline for Blue Ocean: Version 1.27.3
* GitHub plugin: Version 1.37.0
* Green Balls: Version 1.15.1
* i18n for Blue Ocean: Version 1.27.3
* Javadoc: Version 226.v71211feb_e7e9
* Job Configuration History Plugin: Version 1207.vd28a_54732f92
* Job DSL: Version 1.83
* JUnit Plugin: Version 1189.v1b_e593637fa_e
* Maven Integration plugin: Version 3.21
* Monitoring: Version 1.92.0
* Oracle Java SE Development Kit Installer Plugin: Version 63.v62d2fd4b_4793
* Personalization for Blue Ocean: Version 1.27.3
* Pipeline: Version 596.v8c21c963d92d
* Pipeline implementation for Blue Ocean: Version 1.27.3
* Pipeline SCM API for Blue Ocean: Version 1.27.3
* Pipeline Utility Steps: Version 2.15.1
* Pipeline: API: Version 1208.v0cc7c6e0da_9e
* Pipeline: Basic Steps: Version 1010.vf7a_b_98e847c1
* Pipeline: Build Step: Version 488.v8993df156e8d
* Pipeline: Declarative: Version 2.2125.vddb_a_44a_d605e
* Pipeline: Declarative Extension Points API: Version 2.2125.vddb_a_44a_d605e
* Pipeline: GitHub: Version 2.8-147.3206e8179b1c
* Pipeline: Job: Version 1289.vd1c337fd5354
* Pipeline: Multibranch: Version 733.v109046189126
* Pipeline: Nodes and Processes: Version 1244.vee71f675dee6
* Pipeline: REST API Plugin: Version 2.32
* Pipeline: SCM Step: Version 408.v7d5b_135a_b_d49
* Pipeline: Stage Step: Version 305.ve96d0205c1c6
* Pipeline: Stage Tags Metadata: Version 2.2125.vddb_a_44a_d605e
* Pipeline: Stage View Plugin: Version 2.32
* Pipeline: Step API: Version 639.v6eca_cd8c04a_a_
* Pipeline: Supporting APIs: Version 839.v35e2736cfd5c
* Plain Credentials Plugin: Version 143.v1b_df8b_d3b_e48
* Plugin Utilities API Plugin: Version 3.2.0
* Publish Over SSH: Version 1.24
* Rebuilder: Version 1.34
* Resource Disposer Plugin: Version 0.22
* REST API for Blue Ocean: Version 1.27.3
* REST Implementation for Blue Ocean: Version 1.27.3
* SCM API Plugin: Version 631.v9143df5b_e4a_a
* Script Security Plugin: Version 1229.v4880b_b_e905a_6
* SSH Agent Plugin: Version 327.v230ecd01f86f
* SSH Build Agents plugin: Version 2.877.v365f5eb_a_b_eec
* SSH Credentials Plugin: Version 305.v8f4381501156
* SSH server: Version 3.275.v9e17c10f2571
* Structs Plugin: Version 324.va_f5d6774f3a_d
* Web for Blue Ocean: Version 1.27.3
* Workspace Cleanup Plugin: Version 0.45

## Makefile
El fichero _Makefile_ contiene los siguientes targets:

* **compile**: Compilación de las clases java correspondientes al proyecto.
* **run**: Ejecución del programa principal
* **test**: Ejecución de tests de prueba unitarios.
* **clean**: Eliminación de ficheros compilados y paquetes generados.
* **package**: Generación de paquete _jar_ para el proyecto en _Java_.
* **all**: Limpieza, ejecución y generación de artefactos.

Estos targets se valen del comando _mvn_ (Maven) con la acción específica.

## Gestión de secretos
Los secretos para acceder a los nodos agentes de _Jenkins_, consultar el fichero [nodo_jenkins.doc](./nodo_jenkins.doc).

Adicionalmente, deberemos especificar las credenciales para acceder al repositorio Github donde tenemos el proyecto. Este repositorio es privado, por lo cual deberemos especificar las credenciales para acceder a Github. Por lado de Github, deberemos crear un token de tipo "classic" siguiendo estas instrucciones: [definicion de tokens personales](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)

Deberemos prestar atención a:
![token personal](images/access_token.png)

Una vez hemos configurado dicho token, deberemos crear las credenciales correspondientes en Jenkins. Crearemos credenciales de sistema con usuario y contraseña.

![Jenkins credentials Github](images/jenkins-credentials.png)

![Jenkins credentials Github](images/jenkins-credentials2.png)

## Pipeline
El Pipeline se define en el fichero _Jenkinsfile_. Se han definido pipelines distintos para las ramas de *dev* y *main*. En la rama *main* se añade el *Continuous Deployment*, desplegando directamente en producción (podría haber añadido una aprobación manual, pero para eso tenemos el flujo de *dev*, donde simplemente se generan los artefactos una vez testeados y habría un proceso manual de despliegue - en este caso, una ejecución en el entorno de *prod*). Sin embargo, podríamos añadir un proceso manual (como vimos en clase) antes de que el pipeline despliegue en producción.

En la rama *dev* se contemplan los entornos de *dev* y de *test* (en base a las acciones destinadas a cada uno de estos entornos), pero no hay interacción con el entorno de *prod*.

### Pipeline (main)

![Pipeline main](images/pipeline-main.png)

Se definen las siguientes fases (utilizan la rama _main_):

1. Build: Fase de compilación y construcción del paquete. El paquete resultante se almacena para fases posteriores. Esta fase se ejecuta en el entorno/nodo _dev_.
2. Unit tests: Se recupera el paquete de la fase 1 y se ejecutan los tests unitarios en el entorno/nodo _dev_.
3. Test: engloba dos acciones paralelas, ambas ejecutadas en el entorno/nodo _test_

* Executing: Se recupera el paquete generado en la fase 1 y se ejecuta.
* Test: Se recupera el paquete generado en la fase 1 y se ejecutan los tests unitarios

4. Production: En el entorno/nodo _prod_, se recupera el paquete generado en la fase 1 y se ejecuta la aplicación.

Adicionalmente, una vez el proceso haya sido exitoso, se guardan los artefactos desplegados. En cualquier caso (sea o no exitoso), se publican los resultados de los tests.

Como vemos, en esta fase se despliega en producción de forma automática. Se podría añadir una acción manual previo paso al despliegue a producción, pero no lo hemos hecho porque consideramos que no es relevante (el despliegue simplemente copia el paquete a producción y ejecuta la aplicación, pero como la aplicación es tan simple y ha sido probada anteriormente, no lo hemos considerado oportuno).

El *Jenkinsfile* correspondiente se define a continuación:

```
pipeline {
  agent any
  stages {
    stage('Build') {
      agent {
        node {
          label 'dev'
        }

      }
      steps {
        echo 'Compiling code...'
        sh '''mvn clean
mvn package'''
        stash(name: 'targetpackage', includes: 'target/my-app*.jar')
      }
    }

    stage('Unit tests') {
      agent {
        node {
          label 'dev'
        }

      }
      steps {
        echo 'Running unit tests...'
        unstash 'targetpackage'
        sh 'mvn test'
      }
    }

    stage('Test') {
      parallel {
        stage('Executing') {
          agent {
            node {
              label 'test'
            }

          }
          steps {
            echo 'Test execution...'
            unstash 'targetpackage'
            sh 'java -jar $(find target | grep my-app)'
          }
        }

        stage('Test') {
          agent {
            node {
              label 'test'
            }

          }
          steps {
            echo 'Running tests in test environment...'
            unstash 'targetpackage'
            sh 'mvn test'
          }
        }

      }
    }

   stage('Production') {
     agent {
       node {
         label 'prod'
       }

     }
     steps {
       echo 'Running in production...'
       unstash 'targetpackage'
       sh 'java -jar $(find . | grep my-app)'
     }
    }
  }
  post {
    success {
      unstash 'targetpackage'
      archiveArtifacts(artifacts: 'target/my-app*.jar', caseSensitive: true)
    }
    always {
      unstash 'targetpackage'
      sh 'mvn surefire-report:report'
      junit 'target/surefire-reports/*.xml'
    }
  }
}
```

### Pipeline (dev)

![Pipeline dev](images/pipeline-dev.png)

Se definen las siguientes fases (subconjunto de las fases del main, pero sobre la rama _dev_):

1. Build: Fase de compilación y construcción del paquete. El paquete resultante se almacena para fases posteriores. Esta fase se ejecuta en el entorno/nodo _dev_.
2. Unit tests: Se recupera el paquete de la fase 1 y se ejecutan los tests unitarios en el entorno/nodo _dev_.
3. Test: engloba dos acciones paralelas, ambas ejecutadas en el entorno/nodo _test_

* Executing: Se recupera el paquete generado en la fase 1 y se ejecuta.
* Test: Se recupera el paquete generado en la fase 1 y se ejecutan los tests unitarios

Adicionalmente, una vez el proceso haya sido exitoso, se guardan los artefactos desplegados. En cualquier caso, se publican los resultados de los tests.

Como vemos, en esta fase NO se despliega en producción, simplemente se dejan listos los artefactos listos para desplegar a producción.

El *Jenkinsfile* correspondiente se define a continuación:

```
pipeline {
  agent any
  stages {
    stage('Build') {
      agent {
        node {
          label 'dev'
        }

      }
      steps {
        echo 'Compiling code...'
        sh '''mvn clean
mvn package'''
        stash(name: 'targetpackage', includes: 'target/my-app*.jar')
      }
    }

    stage('Unit tests') {
      agent {
        node {
          label 'dev'
        }

      }
      steps {
        echo 'Running unit tests...'
        unstash 'targetpackage'
        sh 'mvn test'
      }
    }

    stage('Test') {
      parallel {
        stage('Executing') {
          agent {
            node {
              label 'test'
            }

          }
          steps {
            echo 'Test execution...'
            unstash 'targetpackage'
            sh 'java -jar $(find target | grep my-app)'
          }
        }

        stage('Test') {
          agent {
            node {
              label 'test'
            }

          }
          steps {
            echo 'Running tests in test environment...'
            unstash 'targetpackage'
            sh 'mvn test'
          }
        }

      }
    }

  }
  post {
    success {
      unstash 'targetpackage'
      archiveArtifacts(artifacts: 'target/my-app*.jar', caseSensitive: true)
    }
    always {
      unstash 'targetpackage'
      sh 'mvn surefire-report:report'
      junit 'target/surefire-reports/*.xml'
    }

  }
}
```


### Pipeline artifacts
Podemos ver los artefactos generados (y descargarlos), como se muestra a continuación:
![Pipeline artifacts](images/pipeline-artifacts.png)

### Pipeline tests
Podemos ver los resultados de los tests, como se muestra a continuación:
![Pipeline tests](images/pipeline-tests.png)

## Acción recurrente
Para la definición de la tarea recurrente, se ha definido un Job DSL de Jenkins.
Para ello, he seguido estos pasos:

1. En el Dashboard de Jenkins, en el menú de la izquierda, he seleccionado "New Item"
2. Introduzco "CronJob" como nombre y selecciono "Freestyle project"
3. Pincho en "OK"
4. Selecciono el Job seleccionado y le damos a "Configure"
5. Especifico la siguiente configuración: ![JobDSL](images/jobdsl.png)
6. En "Manage Jenkins", selecciono "In-process script approval" y apruebo el script que acabo de crear
7. Me voy al job "CronJob" y, a la izquierda, pincho "Build Now". Eso nos genera un proyecto "run-tests"
8. El proyecto "run-tests" es el que ejecuta las tareas de test en el intervalo especificado

Veremos los siguientes proyectos:

* mvn-java-example: donde se define el pipeline
* CronJob: donde hemos definido la tarea recurrente
* run-tests: que ejecuta las acciones en cuestión en el tiempo indicado.
  
![Proyectos](images/projects.png)