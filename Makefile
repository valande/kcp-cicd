all:	clean compile test run
compile:
	@echo "Compiling code..."
	mvn compiler:compile
run:
	@echo "Running executable..."
	mvn exec:java
test:
	@echo "Running tests..."
	mvn test
clean:
	@echo "Cleaning artifacts..."
	mvn clean
